package ex1;

public class Base {
    public float n;

    public Base(float n) {
        this.n = n;
    }

    public void f(int g) {

    }
}

class L extends Base {
    private long t;
    public M m;

    public L(float n, long t) {
        super(n);
    }

    public void f() {
    }
}

class M {
    public M() {

    }
}

class A {
    M m;

    public A() {
        m = new M();
    }

    public void metA() {

    }

}

class B {
    public M m;

    public B(M m) {
        this.m = m;
    }

    public void metB() {

    }
}

class X {

    public void i(L I) {

    }
}

