package ex2;

import javax.swing.*;

public class Window extends JFrame {
    public Window() {
        this.setLayout(null);
        this.setSize(500, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Calculator");

        JTextField number1 = new JTextField();
        number1.setBounds(20, 20, 100, 20);
        JTextField number2 = new JTextField();
        number2.setBounds(20, 100, 100, 20);
        JTextField number3 = new JTextField();
        number3.setBounds(130, 200, 100, 20);
        number3.setEditable(false);

        JButton increment = new JButton("Calculate");
        increment.setBounds(20, 400, 100, 20);
        increment.addActionListener(e -> {
            float nr1 = Float.parseFloat(number1.getText());
            float nr2 = Float.parseFloat(number2.getText());
            float nr3 = nr1 + nr2;
            int nr3i = Math.round(nr3);
            if (nr3 == nr3i) {

                int nr3int = Math.round(nr3);
                String value = String.valueOf(nr3int);
                number3.setText(value);
            }
        });

        this.add(number1);
        this.add(number2);
        this.add(number3);
        this.add(increment);
        this.setVisible(true);

    }

    public static void main(String[] args) {
        new Window();
    }
}
